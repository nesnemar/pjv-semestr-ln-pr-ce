package fel.cvut.cz;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;



public class App extends Application {

    Stage window;
    Scene menuScene;
    Scene gameScene;

    @Override
    public void start(Stage primaryStage) {
        window = primaryStage;
        window.setOnCloseRequest(e -> closeProgram());
        window.setTitle("Stinky");

        //-------------------mainPage-----------------------

        Label text = new Label("Stinky");

        Button b1 = new Button("Start game");
        Button b2 = new Button("Load game");
        Button b3 = new Button("Quit game");

        b1.setOnMouseClicked(e -> window.setScene(gameScene));
        //b1.setOnMouseEntered(e -> b1.getStyleClass().add("button-over"));
        b3.setOnMouseClicked(e -> closeProgram());

        VBox vBox = new VBox(text, b1, b2, b3);
        vBox.setSpacing(20);
        vBox.setAlignment(Pos.CENTER);

        BorderPane bp = new BorderPane();
        bp.setCenter(vBox);

        menuScene = new Scene(bp, 640, 480);
        menuScene.getStylesheets().add("textStyle.css");

        //-------------------gameScene---------------------

        Button gB1 = new Button("Welcome in the game!");
        gB1.setOnMouseClicked(e -> window.setScene(menuScene));

        Image image = new Image("knight.png");
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(100);
        imageView.setFitWidth(100);

        VBox gBox = new VBox(gB1);
        gBox.setAlignment(Pos.CENTER);

        HBox hBox = new HBox();
        hBox.getChildren().add(imageView);

        BorderPane gbp = new BorderPane();
        gbp.setCenter(gBox);
        gbp.setBottom(hBox);


        gameScene = new Scene(gbp, 640, 480);
        gameScene.getStylesheets().add("textStyle.css");

        //-----------------------------------------------------

        window.setScene(menuScene);
        window.show();
    }

    private void closeProgram() {
        System.out.println("Game closed successfully.");
        window.close();
    }

    public static void main(String[] args) {
        launch();
    }
}